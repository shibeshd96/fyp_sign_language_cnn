My final year project on sign language recognition

## Requirements
- Python V3
- Tensorflow > 1.5
- Keras, OpenCV (3.4)
- h5py
- Pillow Library
- A good Webcam and a neutral (preferably white) background.

By adding a test gesture folder with black histograms, the accuracy of the program increased substatially from the low 90s to the high 90%.